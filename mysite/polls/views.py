from django.shortcuts import redirect, render
from django.http import HttpResponse
from .forms import CreatePollForm
from .models import Poll 
from django.views import View


class Home(View):
    def get(self,request):
        polls = Poll.objects.all()
        context = {
            'polls':polls
        }
        return render(request,'polls/home.html',context)


# def home(request):
#     polls = Poll.objects.all()
#     context = {
#         'polls':polls
#     }
#     return render(request,'polls/home.html',context)

class Create(View):
    def post(self,request):
        form = CreatePollForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    def get(self,request):
        form = CreatePollForm()
        context = {
            'form': form
            }
        return render(request,'polls/create.html',context)

    
# def create(request):
#     if request.method == 'POST':
#         form = CreatePollForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('home')
#     else:
#         form = CreatePollForm()
#     context = {
#         'form': form
#         }
#     return render(request,'polls/create.html',context)

class Vote(View):
    form_class = CreatePollForm
    modal =Poll
    template_name = 'polls/vote.html'

    def get(self,request,poll_id):
        # poll = Poll.objects.get(pk = poll_id)
        try:
            poll = Poll.objects.get(pk = poll_id)
        except Poll.DoesNotExist:
            poll = None

        context = {
                'poll' : poll
            }
        return render(request,'polls/vote.html',context)
    def post(self,request,poll_id):
        # try: 
            poll = Poll.objects.get(pk=poll_id)
            # if request.method == 'POST':
            selected_option = request.POST['poll']
            if selected_option == 'option1':
                poll.option_one_count += 1
            elif selected_option == 'option2':
                poll.option_two_count += 1
            elif selected_option == 'option3':
                poll.option_three_count += 1
            else:
                return HttpResponse(400, 'Invalid Form')
            poll.save()
            return redirect('results',poll_id)
            
        # except Poll.DoesNotExist:
        #     poll = None
        
    # def get(self,request,poll_id):
        
            

        
    
       


# def vote(request,poll_id):  
#     try: 
#         poll = Poll.objects.get(pk=poll_id)
#         if request.method == 'POST':
#             selected_option = request.POST['poll']
#             if selected_option == 'option1':
#                 poll.option_one_count += 1
#             elif selected_option == 'option2':
#                 poll.option_two_count += 1
#             elif selected_option == 'option3':
#                 poll.option_three_count += 1
#             else:
#                 return HttpResponse(400, 'Invalid Form')
#             poll.save()
#             return redirect('results',poll_id)
#     except Poll.DoesNotExist:
#         poll = None
        
        
#     context = {
#         'poll' : poll
#     }
#     return render(request,'polls/vote.html',context)
    
class Result(View):
    def get(self,request,poll_id):
        try:
            poll = Poll.objects.get(pk = poll_id)
        except Poll.DoesNotExist:
            poll = None
        
        context = {
            'poll':poll
        }
        return render(request,'polls/results.html',context)


# def results(request,poll_id):
#     try:
#         poll = Poll.objects.get(pk = poll_id)
#     except Poll.DoesNotExist:
#         poll = None
        
#     context = {
#         'poll':poll
#     }
#     return render(request,'polls/results.html',context)
