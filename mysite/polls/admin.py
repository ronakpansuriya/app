from django.contrib import admin
from .models import Poll


admin.site.site_header = "Poll App"
# Register your models here.

class AdminPoll(admin.ModelAdmin):
    #display list
    # list_display = ('question' , 'option_one')
    #show filter record
    # list_filter = ('question',)
    pass

admin.site.register(Poll,AdminPoll)