from django.urls import path
from . import views
# from .views import Create,Home
from polls import views

urlpatterns = [
#    path('',views.home, name = 'home'),
#    path('create/',views.create, name='create'),
#    path('results/<poll_id>/',views.results,name='results'),
#    path('vote/<poll_id>/',views.vote,name='vote'),
    path('',views.Home.as_view(),name = 'home'),
    path('create/',views.Create.as_view(), name= 'create'),
    path('vote/<poll_id>/',views.Vote.as_view(),name='vote'),
    path('results/<poll_id>/',views.Result.as_view(),name='results'),
    

   ]
